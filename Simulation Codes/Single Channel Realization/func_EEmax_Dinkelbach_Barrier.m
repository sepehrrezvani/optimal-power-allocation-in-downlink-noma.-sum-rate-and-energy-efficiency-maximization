function [q,p,r,F,lambda_matrix,barrier_func]=func_EEmax_Dinkelbach_Barrier(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,...
        ALPHA,BETA,eps_N,epsilon_B,stepsize,tt,Max_Newton)
lambda=10; %Initialization: Finding lambda such that F(lambda,p)>=0
[q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,...
        Modify_order,cluster_head,P_max,P_mask,Q_min); %Intra-cluster power allocation
[p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
[r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
q_sumrate=q; p_sumrate=p; r_sumrate=r; %%Save
while (sum(r) - lambda*(sum(q)+P_C) <= 0)
    lambda=lambda/2; %Reduce lambda until f(lambda_0,p)>=0
end
%%Dinkelbach Algorithm
%H_n
for n=1:N
    H(n)=alpha(n)*h(cluster_head(n),n);
end
%Determine Q_min_tilde P_mask_tilde P_max_tilde
Q_min_tilde=Q_min-c./alpha;
P_mask_tilde=P_mask-c./alpha;
P_max_tilde=P_max-sum(c./alpha);
iter_Dinkel=1;%Dinkelbach iteration index
F=0;
F(iter_Dinkel)=(sum(r) - lambda*(sum(q)+P_C));
lambda_matrix=0;
lambda_matrix(1,iter_Dinkel)=lambda; %saving matrix of lambda over iterations
barrier_func=[];
while F(iter_Dinkel)>eps_dinkel
    iter_Dinkel=iter_Dinkel+1; %update Dinkelbach iteration index
    %%%%%Update lambda
    [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
    [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
    lambda=sum(r)/(sum(q)+P_C);
    lambda_matrix(1,iter_Dinkel)=lambda;
    %%%%%Find optimal q
    %calculate phi(n)
    phi=zeros(1,N);
    for n=1:N
        phi(n)=1/(log(2)*lambda) - (1-c(n)*h(cluster_head(n),n))/(alpha(n)*h(cluster_head(n),n)) - P_mask(n);
    end
    if (min(phi) >0)
        q=q_sumrate; p=p_sumrate; r=r_sumrate; %solving sum-rate maximization problem
    else
        %Solve via Barrier method
        %% Barrier Method with Inner Newton's method
        %Initializing q_tilde
        q_tilde=q-c./alpha;
        for n=1:N
            q_tilde(n)=max(Q_min_tilde(n),min(0.1*q_tilde(n),P_mask_tilde(n)));
        end
        iterate_barrier=0; %Initializing the counter of barrier iterations
        obj_func_barrier=[];
        while (1>0)
            iterate_barrier=iterate_barrier+1; %barrier iteration index
        %     kk=0;
        %     while (1>0)
        %         kk=kk+1;
            for kk=1:Max_Newton
                U=tt*(-sum(log2(1+(q_tilde.*H)))+lambda*sum(q_tilde))-log(-(sum(q_tilde)-P_max_tilde));% computing U(p)
                obj_func_barrier(iterate_barrier,kk)=-sum(U); %objective function in each iteration (save)
                %%%Step a) Set delta_q
                grad_U=tt*( -H./(log(2).*(1+q_tilde.*H)) + lambda ) - 1./(sum(q_tilde)-P_max_tilde); %computing gradient of U
                hessian=tt*diag( (H./(log(2).*(1+q_tilde.*H))).^2 ) + 1./(sum(q_tilde)-P_max_tilde).^2; %Hessian of U
                delta_q= - (inv(hessian)*grad_U')'; %computing delta q
                %%%Steps b and c) Stopping criterion
                lambda_barrier= - sum(delta_q.*grad_U);
                if lambda_barrier/2 <= eps_N 
                    break 
                end
                %%%Step d) Find step size
                l=1;% initialize t
                while ( min(q_tilde+l*delta_q >= Q_min_tilde)==0 | min(q_tilde+l*delta_q <= P_mask_tilde)==0 | ...
                        sum(q_tilde+l*delta_q) >  P_max_tilde) %Finding feasible q_tilde by determining initial l
                    l = BETA*l;
                end
                while ( tt*(-sum(log2(1+((q_tilde+l*delta_q).*H)))+lambda*sum(q_tilde+l*delta_q))-log(-(sum((q_tilde+l*delta_q))-P_max_tilde)) >...
                        U + ALPHA*l*sum(grad_U.*delta_q) )
                    l = BETA*l;
                end

                %%%Step d) Update p
                for n=1:N
                    q_tilde(n) = max(Q_min_tilde(n),min(q_tilde(n)+l*delta_q(n),P_mask_tilde(n)));
                end
            end

            if 1/tt <= epsilon_B 
                break 
            end
            tt=stepsize*tt; %update t
        end
        q=q_tilde+c./alpha;
        [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
        [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
        %Save objective function of barrier et each Dinkelbach iteration
        barrier_func{iter_Dinkel}=obj_func_barrier;
    end
    %Calculate F
    F(iter_Dinkel)=(sum(r) - lambda*(sum(q)+P_C));
end