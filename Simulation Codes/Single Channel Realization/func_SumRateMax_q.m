function [q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,Modify_order,cluster_head,P_max,P_mask,Q_min)
q=zeros(1,N); %initialization
%%%%Calculating \tilde{Q}^{min}_n and \tilde{P}^{mask}_n
%\alpha_n: First calculating intra-cluster power coefficient of each user:
%denoted by 'beta_term'
beta=(2.^(R_min/W_s)-1)./(2.^(R_min/W_s));
coeff_user=zeros(K,N);
for n=1:N
    if numel(activeuser{n})==1
        coeff_user(:,n)=zeros(K,1);
    else
        for k=1:(numel(activeuser{n})) %first consider all the multiplexed users
            i=activeuser{n}(k); %active user index
            if i~=cluster_head(n) %except cluster head user
                coeff_user(i,n)=beta(i);
                for j=1:(numel(activeuser{n}))
                    m=activeuser{n}(j);
                    if Modify_order(m,n)<Modify_order(i,n)
                        coeff_user(i,n)=coeff_user(i,n)*(1-beta(m));
                    end
                end
            end
        end
    end
end
%alpha_n: power coefficient of the clusterhead users
alpha=1-sum(coeff_user,1);
%H_n
for n=1:N
    H(n)=alpha(n)*h(cluster_head(n),n);
end
%c_n: First obtain c_k of each user: 'beta_term' for 'c_k'.
c_user=zeros(K,N);
beta_term=zeros(K,N);
for n=1:N
    if numel(activeuser{n})==1
        c_user(:,n)=zeros(K,1);
    else
        for x=1:(numel(activeuser{n})) %first consider all the multiplexed users
            k=activeuser{n}(x); %active user index
            if k~=cluster_head(n) %except cluster head user
                beta_term=zeros(K,N);
                for y=1:(numel(activeuser{n}))
                    j=activeuser{n}(y);
                    if Modify_order(j,n)<Modify_order(k,n)
                        beta_term(j,n)=beta(j)/h(j,n);
                        for z=1:(numel(activeuser{n}))
                            l=activeuser{n}(z);
                            if Modify_order(l,n)>Modify_order(j,n) && Modify_order(l,n)<Modify_order(k,n)
                                beta_term(j,n)=beta_term(j,n)*(1-beta(l));
                            end
                        end
                    end
                end
                c_user(k,n)=beta(k)*( (1/h(k,n)) - sum(beta_term(:,n)) );
            end
        end
    end
end
%obtain c(n) of each cluster-head user n
c=sum(c_user,1);
%Determine Q_min_tilde P_mask_tilde P_max_tilde
Q_min_tilde=Q_min-c./alpha;
P_mask_tilde=P_mask-c./alpha;
P_max_tilde=P_max-sum(c./alpha);

if N==1
    q=min(P_mask,P_max);
else
    %Find initial lower-bound nu_l and upper-bound nu_h
    %lower-bound nu_l
    nu_l=max( (1/log(2))./(P_mask_tilde+(1./H)) );
    for n=1:N
        q_tilde(n) = max( Q_min_tilde(n) , min((1/log(2))/nu_l-1./H(n),P_mask_tilde(n)) );
    end
    while sum(q_tilde)<=P_max_tilde
        nu_l=nu_l/2;
        for n=1:N
            q_tilde(n) = max( Q_min_tilde(n) , min((1/log(2))/nu_l-1./H(n),P_mask_tilde(n)) );
        end
    end
    %upper-bound nu_h
    nu_h=10*nu_l;
    for n=1:N
        q_tilde(n) = max( Q_min_tilde(n) , min((1/log(2))/nu_h-1./H(n),P_mask_tilde(n)) );
    end
    while sum(q_tilde)>=P_max_tilde
        nu_h=2*nu_h;
        for n=1:N
            q_tilde(n) = max( Q_min_tilde(n) , min((1/log(2))/nu_h-1./H(n),P_mask_tilde(n)) );
        end
    end
    %Middle nu_m
    nu_m=(nu_l+nu_h)/2;
    for n=1:N
        q_tilde(n) = max( Q_min_tilde(n) , min((1/log(2))/nu_m-1./H(n),P_mask_tilde(n)) );
    end
    %Bisection:
    epsilon=1e-6;
    while abs(P_max_tilde-sum(q_tilde)) > epsilon
        if sum(q_tilde)<P_max_tilde
            nu_h=nu_m;
        else nu_l=nu_m;
        end
        nu_m=(nu_l+nu_h)/2;
        for n=1:N
            q_tilde(n) = max( Q_min_tilde(n) , min((1/log(2))/nu_m-1./H(n),P_mask_tilde(n)) );
        end
    end
    %Obtain optimal q 
    q=q_tilde+c./alpha;
end
