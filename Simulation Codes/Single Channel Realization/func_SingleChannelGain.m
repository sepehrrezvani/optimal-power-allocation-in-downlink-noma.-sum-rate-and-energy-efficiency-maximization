function h=func_SingleChannelGain(K,Cor_user,BS_Type)
%% Distances
%Distance between users and BSs
Dis_user_BS=abs(Cor_user);
%% 3GPP Model
if (BS_Type==1)
    path_exp=128+37.6*log10(Dis_user_BS/1000)+8*randn(1,K);
else path_exp=140.7+36.7*log10(Dis_user_BS/1000)+8*randn(1,K);
end
var=10.^(-path_exp/10);
%%Rayleigh fading + pathloss channel gain
h = sqrt(var/2).*(randn(1,K)+i*randn(1,K));
h=abs(h).^2; % channel power gain


