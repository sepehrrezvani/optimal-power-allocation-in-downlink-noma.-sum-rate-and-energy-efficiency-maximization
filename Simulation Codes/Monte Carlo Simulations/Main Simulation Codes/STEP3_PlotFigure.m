% set(0,'DefaultFigureVisible','off') %do not display figures
set(0,'DefaultFigureVisible','on'); %display figures
Fixed_user=30; %(Mbps) >>> Fixed number of users
Fixed_minrate=1; %(Mbps) >>> Fixed minimum rate demands
save('STEP3_PlotFigure','Fixed_user','Fixed_minrate')

%% Outage Probability: Fully SC-SIC, FD-NOMA, FDMA
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','R_min_vec','U_NOMA_vec')
load('STEP2_Algorithms_PowMin','Feasible_FSCSIC_SAMPLE','Feasible_FDNOMA_SAMPLE','Feasible_FDMA_SAMPLE')
load('STEP3_PlotFigure','Fixed_user','Fixed_minrate')
R_min_vec=R_min_vec./1e+6; % in terms of Mbps similar to the rate of users

%%Number of users and maximum multiplexed users, for fixed minimum rate demand
    index_Rmin=find(R_min_vec==Fixed_minrate);
    outage_FSCSIC=(1-sum(Feasible_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample)';
    outage_FDMA=(1-sum(Feasible_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample)';
    for index_UNOMA=1:numel(U_NOMA_vec)
        outage_FDNOMA{index_UNOMA}=(1-sum(Feasible_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample)';
    end
    Fig_outage_usernum=figure;
    hold on
    plot(K_vector,outage_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,outage_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC',...
           'FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA',...
           'location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Outage probability','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'YScale','log','FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_outage_usernum,'Fig_outage_usernum')

%%Minimum rate demand and maximum multiplexed users, for a fixed user number
    index_Kvec=find(K_vector==Fixed_user);
    outage_FSCSIC=(1-sum(Feasible_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample);
    outage_FDMA=(1-sum(Feasible_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample);
    for index_UNOMA=1:numel(U_NOMA_vec)
        outage_FDNOMA{index_UNOMA}=(1-sum(Feasible_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample);
    end
    Fig_outage_minrate=figure;
    hold on
    plot(R_min_vec,outage_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,outage_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC',...
           'FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA',...
           'location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Outage probability','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'YScale','log','FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_outage_minrate,'Fig_outage_minrate')

%% Minimum Power Consumption (dBm)
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','P_max','R_min_vec','U_NOMA_vec')
load('STEP2_Algorithms_PowMin','P_powmin_FSCSIC_SAMPLE','P_powmin_FDNOMA_SAMPLE','P_powmin_FDMA_SAMPLE')
load('STEP3_PlotFigure','Fixed_user','Fixed_minrate')
R_min_vec=R_min_vec./1e+6; % in terms of Mbps similar to the rate of users

%%Number of users and maximum multiplexed users, for fixed minimum rate demand
    index_Rmin=find(R_min_vec==Fixed_minrate);
    P_FSCSIC=sum(10*log10(P_powmin_FSCSIC_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_powmin_FDMA_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_powmin_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:))+30,4)./channel_sample;
    end
    Fig_PM_usernum=figure;
    hold on
    plot(K_vector,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(K_vector,(10*log10(P_max)+30)*ones(1,numel(K_vector)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average total power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_PM_usernum,'Fig_PM_usernum')
    
%%Minimum rate demand and maximum multiplexed users, for a fixed user number
    index_Kvec=find(K_vector==Fixed_user);
    P_FSCSIC=sum(10*log10(P_powmin_FSCSIC_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_powmin_FDMA_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_powmin_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:))+30,4)./channel_sample; %dBm
    end
    Fig_PM_minrate=figure;
    hold on
    plot(R_min_vec,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(R_min_vec,(10*log10(P_max)+30)*ones(1,numel(R_min_vec)),'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',4,'Marker','o','linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average total power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_PM_minrate,'Fig_PM_minrate')
    
%% Maximum Sum-Rate: Fully SC-SIC, FD-NOMA, FDMA
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','R_min_vec','U_NOMA_vec')
load('STEP2_Algorithms_SumRateMax','R_tot_sumrate_FSCSIC_SAMPLE','R_tot_sumrate_FDNOMA_SAMPLE','R_tot_sumrate_FDMA_SAMPLE')
load('STEP3_PlotFigure','Fixed_user','Fixed_minrate')
R_min_vec=R_min_vec./1e+6; %in terms of Mbps similar to the rate of users

%%Number of users and maximum multiplexed users, for fixed minimum rate demand
    index_Rmin=find(R_min_vec==Fixed_minrate);
    R_tot_FSCSIC=1e-6*sum(R_tot_sumrate_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    R_tot_FDMA=1e-6*sum(R_tot_sumrate_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_sumrate_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    Fig_SR_usernum=figure;
    hold on
    plot(K_vector,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_SR_usernum,'Fig_SR_usernum')

%%Minimum rate demand and maximum multiplexed users, for a fixed user number
    index_Kvec=find(K_vector==Fixed_user);
    R_tot_FSCSIC=1e-6*sum(R_tot_sumrate_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    R_tot_FDMA=1e-6*sum(R_tot_sumrate_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_sumrate_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    Fig_SR_minrate=figure;
    hold on
    plot(R_min_vec,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_SR_minrate,'Fig_SR_minrate')

%% Maximum Energy Efficiency (EE): Fully SC-SIC, FD-NOMA, FDMA - Power Consumption
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','P_max','R_min_vec','U_NOMA_vec')
load('STEP2_Algorithms_EEMax','P_tot_EE_FSCSIC_SAMPLE','P_tot_EE_FDNOMA_SAMPLE','P_tot_EE_FDMA_SAMPLE')
load('STEP3_PlotFigure','Fixed_user','Fixed_minrate')
R_min_vec=R_min_vec./1e+6; % in terms of Mbps similar to the rate of users

%%Number of users and maximum multiplexed users, for fixed minimum rate demand
    index_Rmin=find(R_min_vec==Fixed_minrate);
    P_FSCSIC=sum(10*log10(P_tot_EE_FSCSIC_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_tot_EE_FDMA_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_tot_EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:))+30,4)./channel_sample;
    end
    Fig_EEPM_usernum=figure;
    hold on
    plot(K_vector,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(K_vector,(10*log10(P_max)+30)*ones(1,numel(K_vector)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EEPM_usernum,'Fig_EEPM_usernum')

%%Minimum rate demand and maximum multiplexed users, for a fixed user number
    index_Kvec=find(K_vector==Fixed_user);
    P_FSCSIC=sum(10*log10(P_tot_EE_FSCSIC_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_tot_EE_FDMA_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_tot_EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:))+30,4)./channel_sample; %dBm
    end
    Fig_EEPM_minrate=figure;
    hold on
    plot(R_min_vec,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(R_min_vec,(10*log10(P_max)+30)*ones(1,numel(R_min_vec)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EEPM_minrate,'Fig_EEPM_minrate')

%% Maximum Energy Efficiency (EE): Fully SC-SIC, FD-NOMA, FDMA - Sum-Rate
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','R_min_vec','U_NOMA_vec')
load('STEP2_Algorithms_EEMax','R_tot_EE_FSCSIC_SAMPLE','R_tot_EE_FDNOMA_SAMPLE','R_tot_EE_FDMA_SAMPLE')
load('STEP3_PlotFigure','Fixed_user','Fixed_minrate')
R_min_vec=R_min_vec./1e+6; %in terms of Mbps similar to the rate of users

%%Number of users and maximum multiplexed users, for fixed minimum rate demand
    index_Rmin=find(R_min_vec==Fixed_minrate);
    R_tot_FSCSIC=1e-6*sum(R_tot_EE_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    R_tot_FDMA=1e-6*sum(R_tot_EE_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    Fig_EESR_usernum=figure;
    hold on
    plot(K_vector,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESR_usernum,'Fig_EESR_usernum')

%%Minimum rate demand and maximum multiplexed users, for a fixed user number
    index_Kvec=find(K_vector==Fixed_user);
    R_tot_FSCSIC=1e-6*sum(R_tot_EE_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    R_tot_FDMA=1e-6*sum(R_tot_EE_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    Fig_EESR_minrate=figure;
    hold on
    plot(R_min_vec,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESR_minrate,'Fig_EESR_minrate')

%% Maximum Energy Efficiency (EE): Fully SC-SIC, FD-NOMA, FDMA - EE Function
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','P_max','R_min_vec','U_NOMA_vec')
load('STEP2_Algorithms_EEMax','EE_FSCSIC_SAMPLE','EE_FDNOMA_SAMPLE','EE_FDMA_SAMPLE')
load('STEP3_PlotFigure','Fixed_user','Fixed_minrate')
R_min_vec=R_min_vec./1e+6; %in terms of Mbps similar to the rate of users

%%Number of users and maximum multiplexed users, for fixed minimum rate demand
    index_Rmin=find(R_min_vec==Fixed_minrate);
    EE_FSCSIC=1e-6*sum(EE_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    EE_FDMA=1e-6*sum(EE_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        EE_FDNOMA{index_UNOMA}=1e-6*sum(EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    Fig_EESYSTEM_usernum=figure;
    hold on
    plot(K_vector,EE_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,EE_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average energy efficiency (Mbps/Joule)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESYSTEM_usernum,'Fig_EESYSTEM_usernum')

%%Minimum rate demand and maximum multiplexed users, for a fixed user number
    index_Kvec=find(K_vector==Fixed_user);
    EE_FSCSIC=1e-6*sum(EE_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    EE_FDMA=1e-6*sum(EE_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        EE_FDNOMA{index_UNOMA}=1e-6*sum(EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    Fig_EESYSTEM_minrate=figure;
    hold on
    plot(R_min_vec,EE_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,EE_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average energy efficiency (Mbps/Joule)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESYSTEM_minrate,'Fig_EESYSTEM_minrate')
    
    
