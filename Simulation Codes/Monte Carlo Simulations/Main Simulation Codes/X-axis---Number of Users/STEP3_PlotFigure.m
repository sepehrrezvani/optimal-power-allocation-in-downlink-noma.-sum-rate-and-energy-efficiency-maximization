% set(0,'DefaultFigureVisible','off') %do not display figures
set(0,'DefaultFigureVisible','on'); %display figures

%% Outage Probability: Fully SC-SIC, FD-NOMA, FDMA: Number of Users
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','U_NOMA_vec')
load('STEP2_Algorithms_PowMin','Feasible_FSCSIC_SAMPLE','Feasible_FDNOMA_SAMPLE','Feasible_FDMA_SAMPLE')

index_Rmin=1;
    outage_FSCSIC=(1-sum(Feasible_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample)';
    outage_FDMA=(1-sum(Feasible_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample)';
    for index_UNOMA=1:numel(U_NOMA_vec)
        outage_FDNOMA{index_UNOMA}=(1-sum(Feasible_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample)';
    end
    
    Fig_outage_usernum1=figure;
    hold on
    plot(K_vector,outage_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,outage_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC',...
           'FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA',...
           'location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Outage probability','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'YScale','log','FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_outage_usernum1,'Fig_outage_usernum1')

index_Rmin=2;
    outage_FSCSIC=(1-sum(Feasible_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample)';
    outage_FDMA=(1-sum(Feasible_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample)';
    for index_UNOMA=1:numel(U_NOMA_vec)
        outage_FDNOMA{index_UNOMA}=(1-sum(Feasible_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample)';
    end
    
    Fig_outage_usernum2=figure;
    hold on
    plot(K_vector,outage_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,outage_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC',...
           'FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA',...
           'location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Outage probability','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'YScale','log','FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_outage_usernum2,'Fig_outage_usernum2')
    
index_Rmin=3;
    outage_FSCSIC=(1-sum(Feasible_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample)';
    outage_FDMA=(1-sum(Feasible_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample)';
    for index_UNOMA=1:numel(U_NOMA_vec)
        outage_FDNOMA{index_UNOMA}=(1-sum(Feasible_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample)';
    end
    
    Fig_outage_usernum3=figure;
    hold on
    plot(K_vector,outage_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,outage_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,outage_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC',...
           'FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA',...
           'location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Outage probability','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'YScale','log','FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_outage_usernum3,'Fig_outage_usernum3')
    
%% Minimum Power Consumption (dBm): Number of Users
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','P_max','U_NOMA_vec')
load('STEP2_Algorithms_PowMin','P_powmin_FSCSIC_SAMPLE','P_powmin_FDNOMA_SAMPLE','P_powmin_FDMA_SAMPLE')

index_Rmin=1;
    P_FSCSIC=sum(10*log10(P_powmin_FSCSIC_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_powmin_FDMA_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_powmin_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:))+30,4)./channel_sample;
    end
    
    Fig_PM_usernum1=figure;
    hold on
    plot(K_vector,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(K_vector,(10*log10(P_max)+30)*ones(1,numel(K_vector)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average total power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_PM_usernum1,'Fig_PM_usernum1')

index_Rmin=2;
    P_FSCSIC=sum(10*log10(P_powmin_FSCSIC_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_powmin_FDMA_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_powmin_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:))+30,4)./channel_sample;
    end
    
    Fig_PM_usernum2=figure;
    hold on
    plot(K_vector,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(K_vector,(10*log10(P_max)+30)*ones(1,numel(K_vector)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average total power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_PM_usernum2,'Fig_PM_usernum2')

index_Rmin=3;
    P_FSCSIC=sum(10*log10(P_powmin_FSCSIC_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_powmin_FDMA_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_powmin_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:))+30,4)./channel_sample;
    end
    
    Fig_PM_usernum3=figure;
    hold on
    plot(K_vector,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(K_vector,(10*log10(P_max)+30)*ones(1,numel(K_vector)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average total power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_PM_usernum3,'Fig_PM_usernum3')

%% Maximum Sum-Rate: Fully SC-SIC, FD-NOMA, FDMA: Number of Users   
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','U_NOMA_vec')
load('STEP2_Algorithms_SumRateMax','R_tot_sumrate_FSCSIC_SAMPLE','R_tot_sumrate_FDNOMA_SAMPLE','R_tot_sumrate_FDMA_SAMPLE')

index_Rmin=1;
    R_tot_FSCSIC=1e-6*sum(R_tot_sumrate_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    R_tot_FDMA=1e-6*sum(R_tot_sumrate_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_sumrate_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_SR_usernum1=figure;
    hold on
    plot(K_vector,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southwest','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_SR_usernum1,'Fig_SR_usernum1')

index_Rmin=2;
    R_tot_FSCSIC=1e-6*sum(R_tot_sumrate_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    R_tot_FDMA=1e-6*sum(R_tot_sumrate_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_sumrate_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_SR_usernum2=figure;
    hold on
    plot(K_vector,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','east','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_SR_usernum2,'Fig_SR_usernum2')

index_Rmin=3;
    R_tot_FSCSIC=1e-6*sum(R_tot_sumrate_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    R_tot_FDMA=1e-6*sum(R_tot_sumrate_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_sumrate_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_SR_usernum3=figure;
    hold on
    plot(K_vector,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','east','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_SR_usernum3,'Fig_SR_usernum3')

%% Maximum Energy Efficiency (EE): Fully SC-SIC, FD-NOMA, FDMA - Power Consumption: Number of Users
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','P_max','U_NOMA_vec')
load('STEP2_Algorithms_EEMax','P_tot_EE_FSCSIC_SAMPLE','P_tot_EE_FDNOMA_SAMPLE','P_tot_EE_FDMA_SAMPLE')

index_Rmin=1;
    P_FSCSIC=sum(10*log10(P_tot_EE_FSCSIC_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_tot_EE_FDMA_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_tot_EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:))+30,4)./channel_sample;
    end
    
    Fig_EEPM_usernum1=figure;
    hold on
    plot(K_vector,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(K_vector,(10*log10(P_max)+30)*ones(1,numel(K_vector)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','west','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EEPM_usernum1,'Fig_EEPM_usernum1')

index_Rmin=2;
    P_FSCSIC=sum(10*log10(P_tot_EE_FSCSIC_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_tot_EE_FDMA_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_tot_EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:))+30,4)./channel_sample;
    end
    
    Fig_EEPM_usernum2=figure;
    hold on
    plot(K_vector,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(K_vector,(10*log10(P_max)+30)*ones(1,numel(K_vector)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EEPM_usernum2,'Fig_EEPM_usernum2')

index_Rmin=3;
    P_FSCSIC=sum(10*log10(P_tot_EE_FSCSIC_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_tot_EE_FDMA_SAMPLE(:,index_Rmin,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_tot_EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:))+30,4)./channel_sample;
    end
    
    Fig_EEPM_usernum3=figure;
    hold on
    plot(K_vector,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(K_vector,(10*log10(P_max)+30)*ones(1,numel(K_vector)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EEPM_usernum3,'Fig_EEPM_usernum3')

%% Maximum Energy Efficiency (EE): Fully SC-SIC, FD-NOMA, FDMA - Sum-Rate: Number of Users
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','U_NOMA_vec')
load('STEP2_Algorithms_EEMax','R_tot_EE_FSCSIC_SAMPLE','R_tot_EE_FDNOMA_SAMPLE','R_tot_EE_FDMA_SAMPLE')

index_Rmin=1;
    R_tot_FSCSIC=1e-6*sum(R_tot_EE_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    R_tot_FDMA=1e-6*sum(R_tot_EE_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESR_usernum1=figure;
    hold on
    plot(K_vector,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southwest','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESR_usernum1,'Fig_EESR_usernum1')

index_Rmin=2;
    R_tot_FSCSIC=1e-6*sum(R_tot_EE_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    R_tot_FDMA=1e-6*sum(R_tot_EE_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESR_usernum2=figure;
    hold on
    plot(K_vector,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','east','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESR_usernum2,'Fig_EESR_usernum2')

index_Rmin=3;
    R_tot_FSCSIC=1e-6*sum(R_tot_EE_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    R_tot_FDMA=1e-6*sum(R_tot_EE_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESR_usernum3=figure;
    hold on
    plot(K_vector,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','east','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESR_usernum3,'Fig_EESR_usernum3')

%% Maximum Energy Efficiency (EE): Fully SC-SIC, FD-NOMA, FDMA - EE Function: Number of Users  
clear all
clc
load('STEP1_Settings','K_vector','channel_sample')
load('STEP2_Algorithms_Settings','U_NOMA_vec')
load('STEP2_Algorithms_EEMax','EE_FSCSIC_SAMPLE','EE_FDNOMA_SAMPLE','EE_FDMA_SAMPLE')

index_Rmin=1;
    EE_FSCSIC=1e-6*sum(EE_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    EE_FDMA=1e-6*sum(EE_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        EE_FDNOMA{index_UNOMA}=1e-6*sum(EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESYSTEM_usernum1=figure;
    hold on
    plot(K_vector,EE_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,EE_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southwest','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average energy efficiency (Mbps/Joule)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESYSTEM_usernum1,'Fig_EESYSTEM_usernum1')

index_Rmin=2;
    EE_FSCSIC=1e-6*sum(EE_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    EE_FDMA=1e-6*sum(EE_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        EE_FDNOMA{index_UNOMA}=1e-6*sum(EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESYSTEM_usernum2=figure;
    hold on
    plot(K_vector,EE_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,EE_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','east','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average energy efficiency (Mbps/Joule)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESYSTEM_usernum2,'Fig_EESYSTEM_usernum2')

index_Rmin=3;
    EE_FSCSIC=1e-6*sum(EE_FSCSIC_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    EE_FDMA=1e-6*sum(EE_FDMA_SAMPLE(:,index_Rmin,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        EE_FDNOMA{index_UNOMA}=1e-6*sum(EE_FDNOMA_SAMPLE(:,index_Rmin,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESYSTEM_usernum3=figure;
    hold on
    plot(K_vector,EE_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(K_vector,EE_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(K_vector,EE_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','east','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Number of users (K)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average energy efficiency (Mbps/Joule)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',K_vector,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESYSTEM_usernum3,'Fig_EESYSTEM_usernum3')
