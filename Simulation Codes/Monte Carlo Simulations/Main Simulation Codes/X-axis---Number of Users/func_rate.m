function [r]=func_rate(K,N,cancel,p,h)
INI=zeros(K,N);
for n=1:N
    for m=1:K
        for m_prn=1:K
            if cancel(m,m_prn,n)==1
                INI(m,n)=INI(m,n)+p(m_prn,n);
            end
        end
    end
end
%Check Feasibility
r=sum(log2( 1 + (p.*h)./(1+INI.*h) ),2)';

