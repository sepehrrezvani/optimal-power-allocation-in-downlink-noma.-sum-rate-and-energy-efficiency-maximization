%% Performance Calculation for each Sample
%differences from general code:
%1) R_min_vec=[x,y,z]*1e+6;;
%% General System Settings
clear all
clc
load('STEP1_Settings','BS_Type','K_vector','h_sample','PSDnoise_samples','channel_sample')

W=5e+6; %wireless bandwidth (Hz)
P_max=BS_Type*46+(1-BS_Type)*30; %BS power in dbm
P_max=10.^((P_max-30)./10); %BS power in Watts
P_C=BS_Type*1+(1-BS_Type)*1; %(W)
P_mask_value=P_max; %No additional limit on the maximum power of the single channel

%Threshold parameters for algorithms
eps_dinkel=1e-6; %stopping criterion for Dinkelbach algorithm
eps_subgrad=1e-3; %stopping criterion for subgradient method
%barrier method initialization
ALPHA=0.1; %alpha in backtracking line search (0<alpha<0.5)
BETA=0.8; %beta in backtracking line search (0<beta<1)
eps_N=1e-3; %accuracy parameter for stopping criterion Newton's (inner) iteration
epsilon_B=1e-3; %accuracy parameter for stopping criterion of Barrier (outer) iteration
stepsize=10; %step size for accuracy parameter in the range 10 � 100
tt=2; %initializing the accuracy parameter of Barrier iteration
Max_Newton=20; %maximum number of Newton's (inner) iteration

R_min_vec=[1.5,3,4.5]*1e+6; %Minimum rate demands (bps): equal among users
U_NOMA_vec=[2,4,6]; %Maximum number of multiplexed users in each cluster

fprintf('***Total Samples***\n\n')
tot_samples=length(K_vector)*channel_sample*numel(R_min_vec)*numel(U_NOMA_vec);
disp(tot_samples)
X = sprintf('---Please wait about %d hours!\n\n',round(tot_samples/(5*3600),3));...
disp(X)
it_index=0; %iteration index of for loops (all scenarios)
for index_Kvec=1:numel(K_vector)
    K=K_vector(index_Kvec); %Number of users
    for index_Rmin=1:numel(R_min_vec)
        R_min=[]; R_min=R_min_vec(index_Rmin)*ones(1,K); %Number of users
        for sample=1:channel_sample
            h_SC=[]; h_SC=h_sample{index_Kvec,sample};
            PSD_users=[]; PSD_users=PSDnoise_samples{index_Kvec,sample}; %PSD of AWGN at users
            %% Fully SC-SIC: Multiplexing all users over a single flat fading channel
            U_max=K; %all users are multiplexed
            N=ceil(K/U_max); %number of clusters (here is 1), or number of resource blocks
            W_s=W/N; %bandwidth of each subchannel
            N0=[]; N0=W_s*repmat(PSD_users',1,N); %AWGN power at users over the single channel
            h=[]; h=repmat(h_SC',1,N); %channel gains
            h=h./N0; %channel gains normalized by noise
            P_mask=[]; P_mask=P_mask_value*ones(1,N);
            %Determine user grouping and active users on each resource block (group)
            rho=ones(K,N);
            activeuser=[];
            for n=1:N
                activeuser{n}=find(rho(:,n)); %Set of active users on each group (or time block)
            end
            %%Decoding Order
            [Decod_order]=func_Decoding_order(K,N,h); %lower number corresponds to lower decoding order
            [cancel]=func_Cancellation(K,N,Decod_order); %'Cancel(m,m_prn,n)=1' means that user 'm_prn' can cancel the signal of user 'm' on resource block 'n'
            [Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order); %Modify ordering numbers from 1 to oder of cluster, and set zero for users do not belong to the cluster set
            cluster_head=[];
            for n=1:N
                cluster_head(n)=find(Modify_order(:,n)==max(Modify_order(:,n))); %Vector of Cluster-head users index
            end
            %%Power Minimization Problem (Feasibility problem)
                [Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser); %Find lower bound Q^{min}_n as well as optimal powers
                P_powmin_FSCSIC=sum(Q_min); % Minimum total power consumption to satisfy minimum rate demands (in Watts)   
                Feasible_FSCSIC=( P_powmin_FSCSIC<=P_max );
                %Saving...
                P_powmin_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=P_powmin_FSCSIC;
                Feasible_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=Feasible_FSCSIC;
            %%Sum-Rate Maximization Problem
                %Initialization...
                    R_tot_sumrate_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=0;
                if Feasible_FSCSIC==1
                    [q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,...
                        Modify_order,cluster_head,P_max,P_mask,Q_min); %Intra-cluster power allocation
                    [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
                    [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
                    R_user_FSCSIC=[]; R_user_FSCSIC=W_s*r;
                    R_tot_FSCSIC=sum(R_user_FSCSIC);
                    %Saving...
                    R_tot_sumrate_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=R_tot_FSCSIC;
                end
            %%Energy Efficiency Maximization Problem
                %Initialization...
                    P_tot_EE_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=P_max;
                    R_tot_EE_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=0;
                    EE_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=0;
            if Feasible_FSCSIC==1
                [q,p,r,F,lambda_matrix,S,nu]=func_EEmax_Dinkelbach_Subgradient(K,N,W_s,R_min,h,activeuser,...
                    Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,eps_subgrad);
                %Saving
                P_tot_EE_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=sum(q);
                R_user_FSCSIC=[]; R_user_FSCSIC=W_s*r;
                R_tot_FSCSIC=sum(R_user_FSCSIC);
                R_tot_EE_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=R_tot_FSCSIC;
                EE_system=R_tot_FSCSIC/(sum(q)+P_C);
                EE_FSCSIC_SAMPLE(index_Kvec,index_Rmin,sample)=EE_system;
            end
            for index_UNOMA=1:numel(U_NOMA_vec)
                %% NOMA with FDMA: User grouping over subchannels (flat fading channels)
                U_NOMA=U_NOMA_vec(index_UNOMA); %Maximum number of multiplexed users in NOMA
                U_max=U_NOMA; %maximum number of multiplexed users
                N=ceil(K/U_max); %number of subchannels (clusters)
                W_s=W/N; %bandwidth of each subchannel
                N0=[]; N0=W_s*repmat(PSD_users',1,N); %AWGN power at users over subchannels
                h=[]; h=repmat(h_SC',1,N); %channel gains
                h=h./N0; %channel gains normalized by noise
                P_mask=[]; P_mask=P_mask_value*ones(1,N);
                %%Decoding Order
                [Decod_order]=func_Decoding_order(K,N,h); %lower number corresponds to lower decoding order
                [cancel]=func_Cancellation(K,N,Decod_order); %'Cancel(m,m_prn,n)=1' means that user 'm_prn' can cancel the signal of user 'm' on resource block 'n'
                %Determine user grouping and active users on each resource block (group)
                if N==1
                    frho=ones(K,N);
                else
                    %%NOMA clustering:
                    [rho]=func_NOMA_Clustering(K,N,Decod_order); %rho(k,n)=1: User k belongs to group n or occupies resource block n
                end
                %Saving...
                rho_FDNOMA_SAMPLE{index_Kvec,index_Rmin,index_UNOMA,sample}=rho;
                for n=1:N
                    activeuser{n}=find(rho(:,n)); %Set of active users on each group
                end
                %%Modify the decoding order based on the adopted 'rho'
                [Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order); %Modify ordering numbers from 1 to oder of cluster, and set zero for users do not belong to the cluster set
                for n=1:N
                    cluster_head(n)=find(Modify_order(:,n)==max(Modify_order(:,n))); %Vector of Cluster-head users index
                end
                %%Power Minimization Problem (Feasibility problem)
                    [Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser); %Find lower bound Q^{min}_n as well as optimal powers
                    P_powmin_FDNOMA=sum(Q_min); % Minimum total power consumption to satisfy minimum rate demands (in Watts)   
                    Feasible_FDNOMA=( P_powmin_FDNOMA<=P_max );
                    %Saving...
                    P_powmin_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=P_powmin_FDNOMA;
                    Feasible_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=Feasible_FDNOMA;
                %%Sum-Rate Maximization Problem
                    %Initialization...
                        R_tot_sumrate_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=0;
                        %for equal inter-cluster power allocation
                        R_tot_sumrate_FDNOMA_EPA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=0;
                    if Feasible_FDNOMA==1
                        %optimal inter-cluster power allocation
                            [q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,...
                                Modify_order,cluster_head,P_max,P_mask,Q_min); %Intra-cluster power allocation
                            [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
                            [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
                            R_user_FDNOMA=[]; R_user_FDNOMA=W_s*r;
                            R_tot_FDNOMA=sum(R_user_FDNOMA);
                            %Saving...
                            R_tot_sumrate_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=R_tot_FDNOMA;
                        %Equal inter-cluster power allocation:
                            q=(P_max/N)*ones(1,N);
                            [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
                            [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
                            R_user_FDNOMA_EPA=[]; R_user_FDNOMA_EPA=W_s*r;
                            R_tot_FDNOMA_EPA=sum(R_user_FDNOMA_EPA);
                            %Saving...
                            R_tot_sumrate_FDNOMA_EPA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=R_tot_FDNOMA_EPA;
                    end
                 %%Energy Efficiency Maximization Problem
                    %Initialization...
                        P_tot_EE_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=P_max;
                        R_tot_EE_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=0;
                        EE_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=0;
                if Feasible_FDNOMA==1
                    [q,p,r,F,lambda_matrix,S,nu]=func_EEmax_Dinkelbach_Subgradient(K,N,W_s,R_min,h,activeuser,...
                        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,eps_subgrad);
                    %Saving
                    P_tot_EE_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=sum(q);
                    R_user_FDNOMA=[]; R_user_FDNOMA=W_s*r;
                    R_tot_FDNOMA=sum(R_user_FDNOMA);
                    R_tot_EE_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=R_tot_FDNOMA;
                    EE_system=R_tot_FDNOMA/(sum(q)+P_C);
                    EE_FDNOMA_SAMPLE(index_Kvec,index_Rmin,index_UNOMA,sample)=EE_system;
                end
                it_index=it_index+1; %complete iteration for calculations
            end
                %% FDMA
                U_max=1; %no interference cancellation >> single user on each resource block
                N=ceil(K/U_max); %number of subchannels (clusters)
                W_s=W/N; %bandwidth of each subchannel
                N0=[]; N0=W_s*repmat(PSD_users',1,N); %AWGN power at users over subchannels
                h=[]; h=repmat(h_SC',1,N); %channel gains
                h=h./N0; %channel gains normalized by noise
                P_mask=[]; P_mask=P_mask_value*ones(1,N);
                %%Decoding Order
                [Decod_order]=func_Decoding_order(K,N,h); %lower number corresponds to lower decoding order
                [cancel]=func_Cancellation(K,N,Decod_order); %'Cancel(m,m_prn,n)=1' means that user 'm_prn' can cancel the signal of user 'm' on resource block 'n'
                %Determine user grouping and active users on each resource block (group)
                rho=eye(K); %each user occupies one resource block with equal length
                %Saving...
                for n=1:N
                    activeuser{n}=find(rho(:,n)); %Set of active users on each group
                end
                %%Modify the decoding order based on the adopted 'rho'
                [Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order); %Modify ordering numbers from 1 to oder of cluster, and set zero for users do not belong to the cluster set
                for n=1:N
                    cluster_head(n)=find(Modify_order(:,n)==max(Modify_order(:,n))); %Vector of Cluster-head users index
                end
                %%Power Minimization Problem (Feasibility problem)
                    [Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser); %Find lower bound Q^{min}_n as well as optimal powers
                    P_powmin_FDMA=sum(Q_min); % Minimum total power consumption to satisfy minimum rate demands (in Watts)   
                    Feasible_FDMA=( P_powmin_FDMA<=P_max );
                    %Saving...
                    P_powmin_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=P_powmin_FDMA;
                    Feasible_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=Feasible_FDMA;
                %%Sum-Rate Maximization Problem
                    %Initialization...
                        R_tot_sumrate_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=0;
                        %for equal inter-cluster power allocation
                        R_tot_sumrate_FDMA_EPA_SAMPLE(index_Kvec,index_Rmin,sample)=0;
                    if Feasible_FDMA==1
                        %optimal inter-cluster power allocation
                            [q,alpha,c]=func_SumRateMax_q(K,N,W_s,R_min,h,activeuser,...
                                Modify_order,cluster_head,P_max,P_mask,Q_min); %Intra-cluster power allocation
                            [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
                            [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
                            R_user_FDMA=[]; R_user_FDMA=W_s*r;
                            R_tot_FDMA=sum(R_user_FDMA);
                            %Saving...
                            R_tot_sumrate_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=R_tot_FDMA;
                        %Equal inter-cluster power allocation:
                            q=(P_max/N)*ones(1,N);
                            [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q); %Optimal power allocation
                            [r]=func_rate(K,N,cancel,p,h); %achievable rate of users in bps/Hz
                            R_user_FDMA_EPA=[]; R_user_FDMA_EPA=W_s*r;
                            R_tot_FDMA_EPA=sum(R_user_FDMA_EPA);
                            %Saving...
                            R_tot_sumrate_FDMA_EPA_SAMPLE(index_Kvec,index_Rmin,sample)=R_tot_FDMA_EPA;
                    end
                %%Energy Efficiency Maximization Problem
                    %Initialization...
                        P_tot_EE_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=P_max;
                        R_tot_EE_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=0;
                        EE_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=0;
                if Feasible_FDMA==1
                    [q,p,r,F,lambda_matrix,S,nu]=func_EEmax_Dinkelbach_Subgradient(K,N,W_s,R_min,h,activeuser,...
                        Modify_order,cluster_head,P_max,P_mask,Q_min,eps_dinkel,P_C,cancel,eps_subgrad);
                    %Saving
                    P_tot_EE_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=sum(q);
                    R_user_FDMA=[]; R_user_FDMA=W_s*r;
                    R_tot_FDMA=sum(R_user_FDMA);
                    R_tot_EE_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=R_tot_FDMA;
                    EE_system=R_tot_FDMA/(sum(q)+P_C);
                    EE_FDMA_SAMPLE(index_Kvec,index_Rmin,sample)=EE_system;
                end
%%
if (it_index/tot_samples)==0.25
    fprintf('*** 25 percentage of samples are calculated! ***\n\n')
elseif (it_index/tot_samples)==0.5
    fprintf('*** 50 percentage of samples are calculated! ***\n\n')
elseif (it_index/tot_samples)==0.75
    fprintf('*** 75 percentage of samples are calculated! ***\n\n')
%     it_index
end
        end
    end
end
fprintf('****** Finished! ******\n\n')
save('STEP2_Algorithms_Settings','W','P_max','P_C','P_mask_value','R_min_vec',...
    'U_NOMA_vec','tot_samples')
save('STEP2_Algorithms_PowMin','P_powmin_FSCSIC_SAMPLE','Feasible_FSCSIC_SAMPLE',...
    'P_powmin_FDNOMA_SAMPLE','Feasible_FDNOMA_SAMPLE',...
    'P_powmin_FDMA_SAMPLE','Feasible_FDMA_SAMPLE')
save('STEP2_Algorithms_SumRateMax','R_tot_sumrate_FSCSIC_SAMPLE','R_tot_sumrate_FDNOMA_SAMPLE',...
    'R_tot_sumrate_FDNOMA_EPA_SAMPLE','R_tot_sumrate_FDMA_SAMPLE','R_tot_sumrate_FDMA_EPA_SAMPLE')
save('STEP2_Algorithms_EEMax','P_tot_EE_FSCSIC_SAMPLE','R_tot_EE_FSCSIC_SAMPLE',...
    'EE_FSCSIC_SAMPLE','P_tot_EE_FDNOMA_SAMPLE','R_tot_EE_FDNOMA_SAMPLE',...
    'EE_FDNOMA_SAMPLE','P_tot_EE_FDMA_SAMPLE','R_tot_EE_FDMA_SAMPLE','EE_FDMA_SAMPLE')