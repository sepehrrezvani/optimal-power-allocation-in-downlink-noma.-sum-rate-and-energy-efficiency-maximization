function [Q_min,p]=func_PowMin(K,N,W_s,R_min,h,Modify_order,activeuser)
p=zeros(K,N);
order_user=Modify_order;
T=(2.^repmat(R_min'/W_s,1,N)-1)./h;
for n=1:N
    cnter=0;
    while (cnter <= numel(activeuser{n})-1)
        str_usr=find(order_user(:,n)==max(order_user(:,n)));
        p(str_usr,n)=T(str_usr,n).*(1 + sum(p(:,n))*h(str_usr,n));
        order_user(str_usr,n)=0;
        cnter=cnter+1;
    end
end
Q_min=sum(p,1);