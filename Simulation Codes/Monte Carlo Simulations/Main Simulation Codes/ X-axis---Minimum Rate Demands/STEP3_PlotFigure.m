% set(0,'DefaultFigureVisible','off') %do not display figures
set(0,'DefaultFigureVisible','on'); %display figures

%% Outage Probability: Fully SC-SIC, FD-NOMA, FDMA: Minimum rate demand
clear all
clc
load('STEP1_Settings','channel_sample')
load('STEP2_Algorithms_Settings','R_min_vec','U_NOMA_vec','specific_usernum_index')
load('STEP2_Algorithms_PowMin','Feasible_FSCSIC_SAMPLE','Feasible_FDNOMA_SAMPLE','Feasible_FDMA_SAMPLE')
R_min_vec=R_min_vec./1e+6; % in terms of Mbps similar to rate of users

index_Kvec=specific_usernum_index(1);
    outage_FSCSIC=[]; outage_FDMA=[]; outage_FDNOMA=[];
    outage_FSCSIC=(1-sum(Feasible_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample);
    outage_FDMA=(1-sum(Feasible_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample);
    for index_UNOMA=1:numel(U_NOMA_vec)
        outage_FDNOMA{index_UNOMA}=(1-sum(Feasible_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample);
    end
    
    Fig_outage_minrate1=figure;
    hold on
    plot(R_min_vec,outage_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,outage_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC',...
           'FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA',...
           'location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Outage probability','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'YScale','log','FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_outage_minrate1,'Fig_outage_minrate1')

index_Kvec=specific_usernum_index(2);
    outage_FSCSIC=[]; outage_FDMA=[]; outage_FDNOMA=[];
    outage_FSCSIC=(1-sum(Feasible_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample);
    outage_FDMA=(1-sum(Feasible_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample);
    for index_UNOMA=1:numel(U_NOMA_vec)
        outage_FDNOMA{index_UNOMA}=(1-sum(Feasible_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample);
    end
    
    Fig_outage_minrate2=figure;
    hold on
    plot(R_min_vec,outage_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,outage_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC',...
           'FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA',...
           'location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Outage probability','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'YScale','log','FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_outage_minrate2,'Fig_outage_minrate2')
    
 index_Kvec=specific_usernum_index(3);
    outage_FSCSIC=[]; outage_FDMA=[]; outage_FDNOMA=[];
    outage_FSCSIC=(1-sum(Feasible_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample);
    outage_FDMA=(1-sum(Feasible_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample);
    for index_UNOMA=1:numel(U_NOMA_vec)
        outage_FDNOMA{index_UNOMA}=(1-sum(Feasible_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample);
    end
    
    Fig_outage_minrate3=figure;
    hold on
    plot(R_min_vec,outage_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,outage_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,outage_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC',...
           'FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA',...
           'location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Outage probability','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'YScale','log','FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_outage_minrate3,'Fig_outage_minrate3')
    
%% Minimum Power Consumption (dBm): Minimum rate demand
clear all
clc
load('STEP1_Settings','channel_sample')
load('STEP2_Algorithms_Settings','P_max','R_min_vec','U_NOMA_vec','specific_usernum_index')
load('STEP2_Algorithms_PowMin','P_powmin_FSCSIC_SAMPLE','P_powmin_FDNOMA_SAMPLE','P_powmin_FDMA_SAMPLE')
R_min_vec=R_min_vec./1e+6; % in terms of Mbps similar to the rate of users

index_Kvec=specific_usernum_index(1);
    P_FSCSIC=sum(10*log10(P_powmin_FSCSIC_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_powmin_FDMA_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_powmin_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:))+30,4)./channel_sample;
    end
    
    Fig_PM_minrate1=figure;
    hold on
    plot(R_min_vec,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(R_min_vec,(10*log10(P_max)+30)*ones(1,numel(R_min_vec)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average total power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_PM_minrate1,'Fig_PM_minrate1')
    
index_Kvec=specific_usernum_index(2);
    P_FSCSIC=sum(10*log10(P_powmin_FSCSIC_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_powmin_FDMA_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_powmin_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:))+30,4)./channel_sample;
    end
    
    Fig_PM_minrate2=figure;
    hold on
    plot(R_min_vec,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(R_min_vec,(10*log10(P_max)+30)*ones(1,numel(R_min_vec)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Minimum Power Consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_PM_minrate2,'Fig_PM_minrate2')
    
index_Kvec=specific_usernum_index(3);
    P_FSCSIC=sum(10*log10(P_powmin_FSCSIC_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_powmin_FDMA_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_powmin_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:))+30,4)./channel_sample;
    end
    
    Fig_PM_minrate3=figure;
    hold on
    plot(R_min_vec,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(R_min_vec,(10*log10(P_max)+30)*ones(1,numel(R_min_vec)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average total power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_PM_minrate3,'Fig_PM_minrate3')
    
%% Maximum Sum-Rate: Fully SC-SIC, FD-NOMA, FDMA: Minimum rate demand
clear all
clc
load('STEP1_Settings','channel_sample')
load('STEP2_Algorithms_Settings','R_min_vec','U_NOMA_vec','specific_usernum_index')
load('STEP2_Algorithms_SumRateMax','R_tot_sumrate_FSCSIC_SAMPLE','R_tot_sumrate_FDNOMA_SAMPLE','R_tot_sumrate_FDMA_SAMPLE')
R_min_vec=R_min_vec./1e+6; % in terms of Mbps similar to the rate of users

index_Kvec=specific_usernum_index(1);
    R_tot_FSCSIC=1e-6*sum(R_tot_sumrate_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    R_tot_FDMA=1e-6*sum(R_tot_sumrate_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_sumrate_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_SR_minrate1=figure;
    hold on
    plot(R_min_vec,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southwest','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_SR_minrate1,'Fig_SR_minrate1')

index_Kvec=specific_usernum_index(2);
    R_tot_FSCSIC=1e-6*sum(R_tot_sumrate_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    R_tot_FDMA=1e-6*sum(R_tot_sumrate_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_sumrate_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_SR_minrate2=figure;
    hold on
    plot(R_min_vec,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southwest','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_SR_minrate2,'Fig_SR_minrate2')

index_Kvec=specific_usernum_index(3);
    R_tot_FSCSIC=1e-6*sum(R_tot_sumrate_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    R_tot_FDMA=1e-6*sum(R_tot_sumrate_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_sumrate_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_SR_minrate3=figure;
    hold on
    plot(R_min_vec,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','east','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_SR_minrate3,'Fig_SR_minrate3')

%% Maximum Energy Efficiency (EE): Fully SC-SIC, FD-NOMA, FDMA - Power Consumption : Minimum rate demand
clear all
clc
load('STEP1_Settings','channel_sample')
load('STEP2_Algorithms_Settings','P_max','R_min_vec','U_NOMA_vec','specific_usernum_index')
load('STEP2_Algorithms_EEMax','P_tot_EE_FSCSIC_SAMPLE','P_tot_EE_FDNOMA_SAMPLE','P_tot_EE_FDMA_SAMPLE')
R_min_vec=R_min_vec./1e+6; % in terms of Mbps similar to the rate of users

index_Kvec=specific_usernum_index(1);
    P_FSCSIC=sum(10*log10(P_tot_EE_FSCSIC_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_tot_EE_FDMA_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_tot_EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:))+30,4)./channel_sample; %dBm
    end
    
    Fig_EEPM_minrate1=figure;
    hold on
    plot(R_min_vec,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(R_min_vec,(10*log10(P_max)+30)*ones(1,numel(R_min_vec)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','west','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EEPM_minrate1,'Fig_EEPM_minrate1')

index_Kvec=specific_usernum_index(2);
    P_FSCSIC=sum(10*log10(P_tot_EE_FSCSIC_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_tot_EE_FDMA_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_tot_EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:))+30,4)./channel_sample; %dBm
    end
    
    Fig_EEPM_minrate2=figure;
    hold on
    plot(R_min_vec,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(R_min_vec,(10*log10(P_max)+30)*ones(1,numel(R_min_vec)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EEPM_minrate2,'Fig_EEPM_minrate2')

index_Kvec=specific_usernum_index(3);
    P_FSCSIC=sum(10*log10(P_tot_EE_FSCSIC_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    P_FDMA=sum(10*log10(P_tot_EE_FDMA_SAMPLE(index_Kvec,:,:))+30,3)./channel_sample; %dBm
    for index_UNOMA=1:numel(U_NOMA_vec)
        P_FDNOMA{index_UNOMA}=sum(10*log10(P_tot_EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:))+30,4)./channel_sample; %dBm
    end
    
    Fig_EEPM_minrate3=figure;
    hold on
    plot(R_min_vec,P_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,P_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,P_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    plot(R_min_vec,(10*log10(P_max)+30)*ones(1,numel(R_min_vec)),'linewidth',2,'Color','r','linestyle','-');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','P^{max}','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average power consumption (dBm)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EEPM_minrate3,'Fig_EEPM_minrate3')

%% Maximum Energy Efficiency (EE): Fully SC-SIC, FD-NOMA, FDMA - Sum-Rate: Minimum rate demand
clear all
clc
load('STEP1_Settings','channel_sample')
load('STEP2_Algorithms_Settings','R_min_vec','U_NOMA_vec','specific_usernum_index')
load('STEP2_Algorithms_EEMax','R_tot_EE_FSCSIC_SAMPLE','R_tot_EE_FDNOMA_SAMPLE','R_tot_EE_FDMA_SAMPLE')
R_min_vec=R_min_vec./1e+6; % in terms of Mbps similar to the rate of users

index_Kvec=specific_usernum_index(1);
    R_tot_FSCSIC=1e-6*sum(R_tot_EE_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    R_tot_FDMA=1e-6*sum(R_tot_EE_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESR_minrate1=figure;
    hold on
    plot(R_min_vec,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESR_minrate1,'Fig_EESR_minrate1')

index_Kvec=specific_usernum_index(2);
    R_tot_FSCSIC=1e-6*sum(R_tot_EE_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    R_tot_FDMA=1e-6*sum(R_tot_EE_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESR_minrate2=figure;
    hold on
    plot(R_min_vec,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southwest','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESR_minrate2,'Fig_EESR_minrate2')

index_Kvec=specific_usernum_index(3);
    R_tot_FSCSIC=1e-6*sum(R_tot_EE_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    R_tot_FDMA=1e-6*sum(R_tot_EE_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        R_tot_FDNOMA{index_UNOMA}=1e-6*sum(R_tot_EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESR_minrate3=figure;
    hold on
    plot(R_min_vec,R_tot_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,R_tot_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,R_tot_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southeast','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('EE: Average sum-rate of users (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESR_minrate3,'Fig_EESR_minrate3')

%% Maximum Energy Efficiency (EE): Fully SC-SIC, FD-NOMA, FDMA - EE Function: Minimum rate demand
clear all
clc
load('STEP1_Settings','channel_sample')
load('STEP2_Algorithms_Settings','R_min_vec','U_NOMA_vec','specific_usernum_index')
load('STEP2_Algorithms_EEMax','EE_FSCSIC_SAMPLE','EE_FDNOMA_SAMPLE','EE_FDMA_SAMPLE')
R_min_vec=R_min_vec./1e+6; % in terms of Mbps similar to the rate of users

index_Kvec=specific_usernum_index(1);
    EE_FSCSIC=1e-6*sum(EE_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    EE_FDMA=1e-6*sum(EE_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        EE_FDNOMA{index_UNOMA}=1e-6*sum(EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESYSTEM_minrate1=figure;
    hold on
    plot(R_min_vec,EE_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,EE_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','southwest','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average energy efficiency (Mbps/Joule)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESYSTEM_minrate1,'Fig_EESYSTEM_minrate1')

index_Kvec=specific_usernum_index(2);
    EE_FSCSIC=1e-6*sum(EE_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    EE_FDMA=1e-6*sum(EE_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        EE_FDNOMA{index_UNOMA}=1e-6*sum(EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESYSTEM_minrate2=figure;
    hold on
    plot(R_min_vec,EE_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,EE_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','east','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average energy efficiency (Mbps/Joule)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESYSTEM_minrate2,'Fig_EESYSTEM_minrate2')

index_Kvec=specific_usernum_index(3);
    EE_FSCSIC=1e-6*sum(EE_FSCSIC_SAMPLE(index_Kvec,:,:),3)./channel_sample;
    EE_FDMA=1e-6*sum(EE_FDMA_SAMPLE(index_Kvec,:,:),3)./channel_sample; 
    for index_UNOMA=1:numel(U_NOMA_vec)
        EE_FDNOMA{index_UNOMA}=1e-6*sum(EE_FDNOMA_SAMPLE(index_Kvec,:,index_UNOMA,:),4)./channel_sample;
    end
    
    Fig_EESYSTEM_minrate3=figure;
    hold on
    plot(R_min_vec,EE_FSCSIC,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
    plot(R_min_vec,EE_FDNOMA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDNOMA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDNOMA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
    plot(R_min_vec,EE_FDMA,'MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
    legend('Fully SC-SIC','FD-NOMA, U^{max}=6','FD-NOMA, U^{max}=4','FD-NOMA, U^{max}=2',...
           'FDMA','location','east','NumColumns',1);
    set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
    xlabel('Minimum rate demand of each user (Mbps)','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Average energy efficiency (Mbps/Joule)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_EESYSTEM_minrate3,'Fig_EESYSTEM_minrate3')