function [p]=func_SumRateMax_p(K,N,W_s,Modify_order,activeuser,h,R_min,q)
p=zeros(K,N);
cnter=0;
for n=1:N
    vec=[];
    vec=Modify_order(:,n);
    vec(vec==0)=2*K;
    Modify_order(:,n)=vec';
end
T=(2.^repmat(R_min'/W_s,1,N)-1)./h;
for n=1:N
    cnter=0;
    %assing power to weaker users (begin from the weakest one
    while (cnter < numel(activeuser{n})-1)
        weak_usr=find(Modify_order(:,n)==min(Modify_order(:,n)));
        p(weak_usr,n)=max(T(weak_usr,n).*(  1 + (q(n)-sum(p(:,n)))*h(weak_usr,n)  )/(2.^(R_min(weak_usr)/W_s)),0);
        Modify_order(weak_usr,n)=2*K;
        cnter=cnter+1;
    end
    %assing power to the cluster-head user
    srng_usr=find(Modify_order(:,n)==min(Modify_order(:,n)));
    p(srng_usr,n)=max(q(n) - sum(p(:,n)),0);
end
