function [cancel] = func_Cancellation_NOMA(K,N,Decod_order)
cancel=zeros(K,K,N);
for n=1:N
    for m=1:K
        for m_prn=1:K
            if(Decod_order(m,n) < Decod_order(m_prn,n))
                cancel(m,m_prn,n)=1;
            end
        end
    end
end
