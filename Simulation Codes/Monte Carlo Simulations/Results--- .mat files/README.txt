---The results for various number of users and minimum rate demands can be downloaded from the following address.
The number of channel realizations for each scenario is 5000. The zip file includes two separated 5000 channel realizations.

https://cloudstorage.tu-braunschweig.de/getlink/fiDJbYJHHgZ7P7qBvhQERTXp/All-5000%20SAMPLES.zip



---The results for specific minimum rate demands can be downloaded from the following address.
The number of channel realizations for each scenario is 50,000.

https://cloudstorage.tu-braunschweig.de/getlink/fi6heKi92hYwXy7hkBMY89uf/X-axis-Minimum%20Rates-50000.zip



---The results for specific number of users can be downloaded from the following address.
The number of channel realizations for each scenario is 50,000.

https://cloudstorage.tu-braunschweig.de/getlink/fiFNg7HwozquRU64J8xAiDVw/X-axis-Number%20of%20Users-50000.zip


The simulation figures in the paper are based on 50,000 channel realizations.