function [Cor_user] = func_CoordinateUser(K,radius_BS,min_Dis_User)
Coordinate=0;
Mokh_user=0;
t=0;
while 6>0 
    Dis_user_BS=min_Dis_User+(radius_BS-min_Dis_User)*unifrnd(0,1);
    angle_user = 2*pi*unifrnd(0,1);
    crd_user=Dis_user_BS.*( cos(angle_user) + i*sin(angle_user) );
    t=t+1;
    Mokh_user=[Mokh_user,crd_user];
    if (t==K)
        break
    end
end
Mokh_user(1)=[]; %Coordinated of users in the cell
Coordinate=[Coordinate,Mokh_user];
Coordinate(1)=[]; %Coordinated of users in the cell
Cor_user=Coordinate;




