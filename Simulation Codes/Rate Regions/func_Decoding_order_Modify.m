function [Modify_order]=func_Decoding_order_Modify(K,N,rho,Decod_order)
Modify_order=zeros(K,N);
for n=1:N
    Cluster_order=sum(rho(:,n)); %number of multiplexed users on each subchannel
    count=Cluster_order;
    while(count > 0)
        stronguser=find(rho(:,n).*Decod_order(:,n)==max(rho(:,n).*Decod_order(:,n)));
        Modify_order(stronguser,n)=count;
        count=count-1;
        Decod_order(stronguser,n)=0;
    end
end