function [Leg_coverage_BS]=func_FigTopology(K,radius_BS,Cor_user)
% %Coverage of the network
% hold on
% t=0:0.1:6.3;
% rad_tot=Dis_BS+max(radius_BS);
% Leg_coverage_tot=plot(rad_tot.*sin(t),rad_tot.*cos(t),'LineStyle','--','Color',[0.8 0 0],'linewidth',0.5);axis equal
%Coverage of BSs

Fig_topology=figure;
hold on
t=0:0.1:6.3;
Leg_coverage_BS=plot(radius_BS.*sin(t),radius_BS.*cos(t),'LineStyle','--','Color',[0 0 0.5],'linewidth',0.01);axis equal

%Location of BSs
Leg_BS=plot(0,0,'MarkerFaceColor',[0 0 0],'MarkerEdgeColor','none',...
    'MarkerSize',10,'Marker','o','LineStyle','none','Color',[0 0 0]);

%Location of users
for m=1:K
    Leg_user=plot(real(Cor_user(1,m)),imag(Cor_user(1,m)),'MarkerFaceColor',[0 0.447058826684952 0.74117648601532],...
        'MarkerEdgeColor',[0 0.447058826684952 0.74117648601532],...
        'MarkerSize',10,...
        'Marker','x',...
        'LineWidth',2,...
        'LineStyle','none',...
        'Color',[0 0 0]);
end
xlabel('Horizontal axis coordinate (m)','FontSize',14,...
    'FontName','Times New Roman')
ylabel('Vertical axis coordinate (m)','FontSize',14,...
    'FontName','Times New Roman')
% Create legend
legend1 = legend([Leg_coverage_BS,Leg_BS,Leg_user],'Coverage area of BS','BS','User');
set(legend1,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])
savefig(Fig_topology,'Fig_topology')
