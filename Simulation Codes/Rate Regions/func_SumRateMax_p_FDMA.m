function [p_cls]=func_SumRateMax_p_FDMA(K,cls,h,rho_NOMA,cluster_head_NOMA,Q_min,q)
cluster_user=rho_NOMA(:,cls);
p_cls=zeros(K,1); %initialization
H=h(cluster_head_NOMA(cls),cluster_head_NOMA(cls)); %subchannel index does not matter since we assumed a flat fading channel
%Find initial lower-bound nu_l and upper-bound nu_h
%lower-bound nu_l
nu_l=max( (1/log(2))./(q(cls)+(1./H)) );
for k=1:K
    p_cls(k) = cluster_user(k) * max( Q_min(k) , (1/log(2))/nu_l - 1/h(k,k) );
end
while sum(p_cls)<q(cls)
    nu_l=nu_l/2;
    for k=1:K
        p_cls(k) = cluster_user(k) * max( Q_min(k) , (1/log(2))/nu_l - 1/h(k,k) );
    end
end
%upper-bound nu_h
nu_h=10*nu_l;
for k=1:K
    p_cls(k) = cluster_user(k) * max( Q_min(k) , (1/log(2))/nu_h - 1/h(k,k) );
end
while sum(p_cls)>q(cls)
    nu_h=2*nu_h;
    for k=1:K
        p_cls(k) = cluster_user(k) * max( Q_min(k) , (1/log(2))/nu_h - 1/h(k,k) );
    end
end
%Middle nu_m
nu_m=(nu_l+nu_h)/2;
for k=1:K
    p_cls(k) = cluster_user(k) * max( Q_min(k) , (1/log(2))/nu_m - 1/h(k,k) );
end
%Bisection:
epsilon=1e-6;
while abs(q(cls)-sum(p_cls)) > epsilon
    if sum(p_cls)<q(cls)
        nu_h=nu_m;
    else nu_l=nu_m;
    end
    nu_m=(nu_l+nu_h)/2;
    for k=1:K
        p_cls(k) = cluster_user(k) * max( Q_min(k) , (1/log(2))/nu_m - 1/h(k,k) );
    end
end




